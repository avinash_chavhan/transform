package com.example;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.FileSystems;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubMessage;
import org.apache.beam.sdk.io.kafka.KafkaIO;
import org.apache.beam.sdk.io.kafka.KafkaRecord;
import org.apache.beam.sdk.io.kafka.KafkaRecordCoder;
import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;

import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Data_Enrichment {
    private static final Logger LOG = LoggerFactory.getLogger(Data_Enrichment.class);

    public static void run(SampleOptions options)
            throws IOException, IllegalArgumentException {
        FileSystems.setDefaultPipelineOptions(options);

        // Create the Pipeline object with the options we defined above.
        Pipeline pipeline = Pipeline.create(options);

        PCollection<KV<String, String>> collection = pipeline.apply(KafkaIO.<String, String>read()
                .withBootstrapServers(options.getKafkaHostname())
                .withTopic(options.getInputTopic())
                .withKeyDeserializer(StringDeserializer.class)
                .withValueDeserializer(StringDeserializer.class)
                .withoutMetadata());

        collection.apply("Test Kafka Record", ParDo.of( new ProcessKafkaFn()));

        pipeline.run().waitUntilFinish();
    }

    public static void main(String[] args) throws IOException, IllegalArgumentException {
        // Create and set your PipelineOptions.
        PipelineOptionsFactory.register(SampleOptions.class);
        SampleOptions options = PipelineOptionsFactory.fromArgs(args).withValidation()
                .as(SampleOptions.class);
        run(options);
    }

    static class ProcessKafkaFn
            extends DoFn<KV<String, String>, String> {

        @ProcessElement
        public void processElement(ProcessContext context) {
            String x = "";
        }
    }
}
